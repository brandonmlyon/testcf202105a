# Project description

## ⚠️ Important links

### [🌐 Live URL](https://distracted-meitner-6d9eb5.netlify.app/)

### [📚 Deliverables & Scope Checklist](https://gitlab.com/brandonmlyon/testcf202105a/-/issues/1)

### [⁉️ What Questions Would I Ask?](https://gitlab.com/brandonmlyon/testcf202105a/-/issues/2)

## 🕹️ Technologies

This demo was built:

* [Using Hugo](https://gohugo.io/)
* [Deployed to Nelify](https://www.netlify.com/)
* [Via GitLab](https://gitlab.com/)

These instructions assume familiarity with:

* Command lines such as Bash or Terminal
* Static site generators such as Hugo
* Tools such as SASS
* Frameworks such as Bulma

### 💻 Running this project locally

###### Clone the [TESTCF202105a git repository](https://gitlab.com/brandonmlyon/testcf202105a)

```
git clone https://gitlab.com/brandonmlyon/testcf202105a.git
```

###### Install Hugo

[How to install Hugo](https://gohugo.io/getting-started/installing/)

###### Run Hugo Server

```
hugo server
```

###### In your browser

The default URL is http://localhost:1313/

## Why?

#### 🎨 Themes

It is often quicker to build using preexisting tools. Starting with a theme isn't always optimal, but it can be efficient.

#### 🖌️ This theme

I searched the Hugo repository, looking for landing page themes. [Hugo Fresh](https://themes.gohugo.io/hugo-fresh/) was a good starting point. There were other options, but this one had the various components I was looking for.

#### 🏗️ Frameworks

Why use a content framework?

###### Pros:

- Frameworks facilitate content creation.
    - Their utilities make it easier for someone who doesn't know CSS to update a page template on their own.
- Using a framework means you can piggyback off of [their documentation](https://bulma.io/documentation/).
- Frameworks facilitate teamwork.
    - A common set of starting rules helps people understand what can be done, what needs to be done, and how to do it.
- Frameworks come with utilities built by experts. If you aren't an expert in accessibility, you can at least piggyback off of the work of people who are experts.

###### Cons:

- Frameworks come with bloat and are not usually DRY.
- Frameworks can be difficult to work with when you're trying to do something they weren't built for.
- Frameworks might be built with different goals in mind than the goals you have. Best practices for product engineering might be contradictory to best practices for conversion optimization.
- Frameworks aren't always easy to modify.


#### 📦 This framework

Bulma came with the theme I chose. It's a framework I haven't used before, but I know it by reputation. I'm more familiar with Bootstrap or Tailwind. 
